using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
   
     GridGenerator gridGenerator; // The GridGenerator script that generates the grid of cubes
    public float moveSpeed = 5f; // The speed at which the cube moves

    private Vector3Int gridPos; // The current grid position of the cube
    private Vector3 targetPos; // The target position for the cube to move to
    private bool canMove = true; // Whether the cube can move or not

    private void Start()
    {
        // Get the initial grid position of the cube
       
    }


    public void SetGridGenerator(GridGenerator generator)
    {
        gridGenerator = generator;
        gridPos = GetGridPosition(transform.position);
        // Set the target position to the current position
        targetPos = transform.position;
    }

    // Change the target position of the cube based on the button press
    public void ChangeDirection(Vector2Int direction)
    {
        if (canMove)
        {
            Vector3Int nextGridPos = gridPos + new Vector3Int(direction.x, 0, direction.y);

            // Check if the next grid position is within the bounds of the grid
            if (nextGridPos.x >= 0 && nextGridPos.x < gridGenerator.gridSize &&
                nextGridPos.z >= 0 && nextGridPos.z < gridGenerator.gridSize)
            {
                // Get the cube at the next grid position
                NormalCube nextCube = gridGenerator.GetCube(nextGridPos.x, nextGridPos.z);

                if (nextCube != null)
                {
                    // Change the material of the next cube to match the player cube's material
                    Material playerMaterial = GetComponent<Renderer>().material;
                    nextCube.GetComponent<Renderer>().material = playerMaterial;

                    // If the next cube has the same material as the player cube, change the material of all the
                    // cubes within the area of the two cubes' materials
                    if (nextCube.GetComponent<Renderer>().material == playerMaterial)
                    {
                        ChangeMaterialsInArea(gridPos, nextGridPos, playerMaterial);
                    }
                }

                // Update the grid position and target position of the player cube
                gridPos = nextGridPos;
                targetPos = gridGenerator.GetCube(gridPos.x, gridPos.z).transform.position;
                canMove = false;
            }
        }
    }

    // Get the grid position of a position in world space
    private Vector3Int GetGridPosition(Vector3 position)
    {
        int x = Mathf.RoundToInt(position.x / (gridGenerator.cubeSize + gridGenerator.spacing));
        int y = Mathf.RoundToInt(position.z / (gridGenerator.cubeSize + gridGenerator.spacing));
        return new Vector3Int(x, 0, y);
    }

    public void ChangeMaterialsInArea(Vector3Int pos1, Vector3Int pos2, Material material)
    {
        int minX = Mathf.Min(pos1.x, pos2.x);
        int maxX = Mathf.Max(pos1.x, pos2.x);
        int minY = Mathf.Min(pos1.z, pos2.z);
        int maxY = Mathf.Max(pos1.z, pos2.z);

        for (int x = minX; x <= maxX; x++)
        {
            for (int y = minY; y <= maxY; y++)
            {
                // Get the cube at the current grid position
                NormalCube cube = gridGenerator.GetCube(x, y);

                if (cube != null)
                {
                    // Change the material of the cube to the specified material
                    cube.GetComponent<MeshRenderer>().material = material;
                }
            }
        }
    }

    private Vector3 moveDir;
    void Update()
    {
         moveDir = Vector3.zero;
        if (Input.GetKey(KeyCode.UpArrow) )
        {
            moveDir = Vector3.up;
        }
        else if (Input.GetKey(KeyCode.DownArrow) )
        {
            moveDir = Vector3.down;
        }
        else if (Input.GetKey(KeyCode.RightArrow) )
        {
            moveDir = Vector3.right;
        }
        else if (Input.GetKey(KeyCode.LeftArrow) )
        {
            moveDir = Vector3.left;
        }

        transform.position += moveDir * 5 * Time.deltaTime;
        
        // Move the cube in its current direction at a constant speed
       

        // Check if the cube is outside the bounds of the grid
        if (!gridGenerator.IsInBounds(transform.position))
        {
            // Reverse the cube's direction if it's outside the bounds of the grid
            moveDir *= -1;

            // Change the color of all the cubes in the same area as the current cube
            ChangeAreaMaterials(GetComponent<MeshRenderer>().material);
        }
    }

    public void ChangeDirection(Vector3Int newDirection)
    {
        // Change the direction of the cube to the specified direction
         moveDir= newDirection;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("GridCube"))
        {
            // Change the color of the other cube to the color of the player cube
            other.gameObject.GetComponent<MeshRenderer>().material = GetComponent<MeshRenderer>().material;

            // Change the color of all the cubes in the same area as the two cubes
            ChangeAreaMaterials(GetComponent<MeshRenderer>().material);
        }
    }

    private void ChangeAreaMaterials(Material material)
    {
        // Get the grid position of the current cube
        Vector3Int gridPos = gridGenerator.GetGridPosition(transform.position);

        // Get the grid position of the cube in the opposite corner of the grid area
        Vector3Int oppositeGridPos = gridGenerator.GetGridPosition(transform.position + moveDir);

        // Change the materials of all the cubes within the area defined by the two grid positions
        ChangeMaterialsInArea(gridPos, oppositeGridPos, material);
    }


   


}
