using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{


    public  Action<SwipeDetector.DraggedDirection> swipeAction;
    
    public Dictionary<Vector2, NormalCube> NormalCubes = new Dictionary<Vector2, NormalCube>();
    public NormalCube cubePrefab; // The prefab to use for each cube
    public int gridSize = 5; // The size of the grid
    public float cubeSize = 1f; // The size of each cube
    public float spacing = 0.1f; // The spacing between each cube

    private NormalCube[,] grid; // The 2D grid of cubes
    public PlayerCube playerCube; // The player cube object
    public PlayerCube playerCubeprefab;


    public static GridGenerator Instance;



    public void AddCubes(NormalCube cube)
    {
       // NormalCubes.Add(cube);
    }

    public void GenerateGrid(int gridSize)
    {
        this.gridSize = gridSize;
        
        grid = new NormalCube[gridSize, gridSize]; // Create the 2D grid

        // Spawn the cubes
        for (int x = 0; x < gridSize; x++)
        {
            for (int y = 0; y < gridSize; y++)
            {
                Vector3 pos = new Vector3(x * (cubeSize + spacing), y * (cubeSize + spacing), 1f);
                grid[x, y] = Instantiate(cubePrefab, pos, Quaternion.identity, transform);
                grid[x, y].SetPos( grid[x, y].transform.localPosition);
                grid[x, y].gridVal = new Vector2(x * (cubeSize + spacing), y * (cubeSize + spacing));
                NormalCubes.Add( grid[x, y].gridVal,grid[x, y]);
            }
        }

        // Spawn the player cube   
        Vector3 playerPos = new Vector3(gridSize / 2 * (cubeSize + spacing),gridSize / 2 * (cubeSize + spacing) , cubeSize / 2f);
        playerCube = Instantiate(playerCubeprefab, playerPos, Quaternion.identity, transform);
        playerCube.gridVal = new Vector2(gridSize / 2 * (cubeSize + spacing),gridSize / 2 * (cubeSize + spacing));
        // playerCube._generator = this;
        playerCube.SetGenerator(this);
        //  playerCube.GetComponent<Cube>().SetGridGenerator(Instance);
        // playerCube.GetComponent<Renderer>().material.color = Color.green; // Set the player cube color
        
        SetBoundaries();
        swipeAction += ChangeDirection;
    }

    private void OnDestroy()
    {
        swipeAction -= ChangeDirection;
    }

    public float playerSpeed = 5f; // Speed of the player cube movement

    public float xMin, xMax, zMin, zMax;
    void SetBoundaries()
    {
        
        xMin = NormalCubes[new Vector2(0, 0)].transform.localPosition.x;
        xMax =  NormalCubes[new Vector2(0, gridSize-1)].transform.localPosition.x;
        zMin = NormalCubes[new Vector2(0, 0)].transform.localPosition.z;
        zMax =  NormalCubes[new Vector2(gridSize-1,0)].transform.localPosition.z;
    }



    void ChangeDirection(SwipeDetector.DraggedDirection val)
    {
        if (val == SwipeDetector.DraggedDirection.Down)
        {
            if(moveDir==Vector3.zero)
                CheckMov();
            moveDir = Vector3.down;
        }
        else if (val == SwipeDetector.DraggedDirection.Up)
        {
            if(moveDir==Vector3.zero)
                CheckMov();
            moveDir = Vector3.up;
        }
        else if (val == SwipeDetector.DraggedDirection.Left)
        {
            if(moveDir==Vector3.zero)
                CheckMov();
            moveDir = Vector3.left;
        }
        else if (val == SwipeDetector.DraggedDirection.Right)
        {
            if(moveDir==Vector3.zero)
                CheckMov();
            moveDir = Vector3.right;
        }
        
    }
    
    
    private Vector3 moveDir;
    public bool left, right, up, down;

    void CheckMov()
    {
        if (moveDir == Vector3.zero && down)
        {
            playerCube.transform.localPosition = new Vector3(playerCube.transform.localPosition.x + 0.1f, playerCube.transform.localPosition.y, playerCube.transform.localPosition.z);
            down = false;
        }
        else if (moveDir == Vector3.zero && up)
        {
            playerCube.transform.localPosition = new Vector3(playerCube.transform.localPosition.x - 0.1f, playerCube.transform.localPosition.y, playerCube.transform.localPosition.z);
            up = false;
        }
        else if (moveDir == Vector3.zero && left)
        {
            playerCube.transform.localPosition = new Vector3(playerCube.transform.localPosition.x, playerCube.transform.localPosition.y, playerCube.transform.localPosition.z + 0.1f);
            left = false;
        }
        else if (moveDir == Vector3.zero && right )
        {
            playerCube.transform.localPosition = new Vector3(playerCube.transform.localPosition.x, playerCube.transform.localPosition.y, playerCube.transform.localPosition.z - 0.1f);
            right = false;
        }
        
    }
    
    

    private void Update()
    {

        if (playerCube.transform.localPosition.x > xMax)
        {
            up = true;
        }
        else
        {
            //  up = false;
        }

        if (playerCube.transform.localPosition.x < xMin)
        {
            down = true;
        }
        else
        {
            //  down = false;
        }

        if (playerCube.transform.localPosition.z > zMax)
        {
            right = true;
        }
        else
        {
            //   right = false;
        }

        if (playerCube.transform.localPosition.z < zMin)
        {
            left = true;
        }
        else
        {
            //    left = false;
        }
        
        if (playerCube.transform.localPosition.x  < xMax && playerCube.transform.localPosition.x > xMin && playerCube.transform.localPosition.z  < zMax && playerCube.transform.localPosition.z  > zMin)
        {
            playerCube.transform.position += moveDir * playerSpeed * Time.deltaTime;
        }
        else
        {
            
            moveDir = Vector3.zero;
        }

    }

    
    
    
    
    private void Awake()
    {
        
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this; 
        } 
        
        
      
    }

    // Get the cube at the given grid coordinates
    public NormalCube GetCube(int x, int y)
    {
        if (x < 0 || x >= gridSize || y < 0 || y >= gridSize)
        {
            return null;
        }

        return grid[x, y];
    }
    
    
    public Vector3Int GetGridPosition(Vector3 worldPos)
    {
        Vector3 localPos = transform.InverseTransformPoint(worldPos);
        Vector3Int gridPos = new Vector3Int(Mathf.RoundToInt(localPos.x), Mathf.RoundToInt(localPos.y), Mathf.RoundToInt(localPos.z));
        return gridPos;
    }

    
    
    public bool IsInBounds(Vector3 gridPos)
    {
        return gridPos.x >= 0 && gridPos.x < gridSize && gridPos.y >= 0 && gridPos.y < gridSize;
    }

}


