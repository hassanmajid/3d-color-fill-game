using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NormalCube : MonoBehaviour
{
    public Vector3 pos;

    public bool selected;

    public Vector2 gridVal;
    public void SetPos(Vector3 val)
    {
        pos = val;
    }

    public Vector3 GetPos()
    {
        return pos;
    }

    public Material cubeMaterial;
    
    public void ChangeMaterial(Material material)
    {
        //
        
        if (gridVal.x >= 0 && gridVal.x < GridGenerator.Instance.gridSize && gridVal.y >= 0 && gridVal.y < GridGenerator.Instance.gridSize)
        {
           // Fill((int)gridVal.x,(int)gridVal.y);
           if (!selected)
           {
             
               ChangeMaterialArea();
               
           }
          
           CheckCubes();
        }
       
       // ReAdjustSizeOfCollider();
       
        
    }
    
    private void Fill(int x, int y) {

        if (!GridGenerator.Instance.NormalCubes.ContainsKey(new Vector2(x, y)))
        {
            return;
          
        }

        if (!GridGenerator.Instance.NormalCubes[new Vector2(x, y)].selected)
        {
           
            
            GridGenerator.Instance.NormalCubes[new Vector2(x, y)].ChangeMaterialArea();
            Fill(x + 1, y);
            Fill(x - 1, y);
            Fill(x, y + 1);
            Fill(x, y - 1);
        }
       
           
    }
    

    public void ChangeMaterialArea()
    {
        
        this.GetComponent<MeshRenderer>().material = cubeMaterial;
        ReAdjustSizeOfCollider();
        selected = true;

        var val = GridGenerator.Instance.playerCube.transform.localPosition;
        var thisObj= this.gameObject.transform.localPosition;


        this.gameObject.transform.DOLocalMove(new Vector3(thisObj.x,val.y,thisObj.z),0.8f);

     
        
        GameManager.Instance.totalCubesPainted++;
        GameManager.Instance.playSound?.Invoke();


        if (GameManager.Instance.totalCubesPainted == (GridGenerator.Instance.gridSize * GridGenerator.Instance.gridSize))
        {
            GameManager.checkifLevelCompleteAction?.Invoke();

        }
    }

    int FindLowerXbound(int x, int y)
    {

        while (x > 0 && !GridGenerator.Instance.NormalCubes[new Vector2(x, y)].selected)
        {
            x--;
        }

        return x;
    }
    
    int FindLowerYbound(int x, int y)
    {

        while (y > 0 && !GridGenerator.Instance.NormalCubes[new Vector2(x, y)].selected)
        {
            y--;
        }

        return y;
    }
    

    void CheckCubes()
    {
        if (GridGenerator.Instance.NormalCubes.ContainsKey(new Vector2(gridVal.x - 1, gridVal.y - 1)))
        {
            // Left arm Check
            if (GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x - 1, gridVal.y)].selected &&
                GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x, gridVal.y - 1)].selected)
            {

                Debug.Log("X Bound is "+ FindLowerXbound((int)gridVal.x-1,(int)gridVal.y-1));
                Debug.Log("Y Bound is "+ FindLowerYbound((int)gridVal.x-1,(int)gridVal.y-1));
                
               
                Fill((int)gridVal.x - 1,(int)gridVal.y-1);
                
                // for (int i = 1; i < gridVal.x; i++)
                // {
                //     for (int j = 1; j < gridVal.y; j++)
                //     {
                //         var val = GridGenerator.Instance.NormalCubes[new Vector2(i, j)];
                //         val.ChangeMaterialArea();
                //     }
                // }

            }
        

                 // Right arm Check
                 if (GridGenerator.Instance.NormalCubes.ContainsKey(new Vector2(gridVal.x + 1, gridVal.y - 1)))
                 {
                     if (GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x + 1, gridVal.y)].selected &&
                         GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x, gridVal.y - 1)].selected)
                     {

                      //   Fill((int)gridVal.x + 1,(int)gridVal.y-1);
                         
                         
                         // for (int i =(int) gridVal.x+1; i < 10; i++)
                         // {
                         //     for (int j = (int)gridVal.y-1; j > 0; j--)
                         //     {
                         //         var val = GridGenerator.Instance.NormalCubes[new Vector2(i,j)];
                         //         val.ChangeMaterialArea();
                         //     }
                         // }

                     }
                 }
                 
                 
                 //Bottom 
                 
                 if (GridGenerator.Instance.NormalCubes.ContainsKey(new Vector2(gridVal.x + 1, gridVal.y + 1)))
                 {
                     if (GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x, gridVal.y+1)].selected &&
                         GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x+1, gridVal.y)].selected)
                     {

                         Fill((int)gridVal.x + 1,(int)gridVal.y+1);
                         
                         
                         // for (int i =(int) gridVal.x+1; i < 10; i++)
                         // {
                         //     for (int j = (int)gridVal.y-1; j > 0; j--)
                         //     {
                         //         var val = GridGenerator.Instance.NormalCubes[new Vector2(i,j)];
                         //         val.ChangeMaterialArea();
                         //     }
                         // }

                     }
                 }
    
                 if (GridGenerator.Instance.NormalCubes.ContainsKey(new Vector2(gridVal.x - 1, gridVal.y + 1)))
                 {
                     if (GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x-1, gridVal.y)].selected &&
                         GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x, gridVal.y+1)].selected)
                     {

                         Fill((int)gridVal.x - 1,(int)gridVal.y+1);
                         
                     }
                 }
                
            
                 if (GridGenerator.Instance.NormalCubes.ContainsKey(new Vector2(gridVal.x + 1, gridVal.y + 1)))
                 {
                     if (GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x, gridVal.y+1)].selected &&
                         GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x+1, gridVal.y)].selected)
                     {

                       //  Fill((int)gridVal.x + 1,(int)gridVal.y+1);
                         
                     }
                 }
            
           // Bottom Left Check
          
               /*if (GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x, gridVal.y+1)].selected && GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x+1 , gridVal.y)].selected )
               {

                   for (int i =(int) gridVal.x+1; i < 10; i++)
                   {
                       for (int j = (int)gridVal.y+1; j < 10; j++)
                       {
                           var val = GridGenerator.Instance.NormalCubes[new Vector2(i,j)];
                           val.ChangeMaterialArea();
                       }
                   }
                
               }*/
          
          
            
            
            
            
        
    }
    }

    
    public void ReAdjustSizeOfCollider()
    {
        this.GetComponent<BoxCollider>().size = new Vector3(1.2f, 1.2f, 1.2f);
    }

    public int triggerval;
    
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("GridCube") && other.gameObject.GetComponent<MeshRenderer>().material == gameObject.GetComponent<MeshRenderer>().material)
        {
            //other.gameObject.GetComponent<NormalCube>().selected
            triggerval++;
            if (triggerval > 3)
            {
                if (GridGenerator.Instance.NormalCubes.ContainsKey(new Vector2(gridVal.x - 1, gridVal.y)))
                {
                    var val=GridGenerator.Instance.NormalCubes[new Vector2(gridVal.x-1,gridVal.y)];
                    val.ChangeMaterialArea();
                }
              
            }
           Debug.Log("Found");
        }
    }
    
    
    
    
    
}
