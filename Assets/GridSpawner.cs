using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSpawner : MonoBehaviour
{
    public int gridWidth = 10; // Number of cubes in a row
    public int gridHeight = 10; // Number of cubes in a column
    public float cubeSize = 1f; // Size of each cube
    public GameObject cubePrefab; // Prefab to use for creating the cubes

    private GameObject playerCube;
    private void Awake()
    {
       SpawnGrid();
       SpawnPlayerCube();
    }



    void SpawnGrid()
    {
        // Loop through each position in the grid and spawn a cube at that position
        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {
                Vector3 cubePos = new Vector3(x * cubeSize, y * cubeSize, 0f);
                GameObject cube = Instantiate(cubePrefab, cubePos, Quaternion.identity, transform);
            }
        }

        // Center the grid in the scene by adjusting the position of the parent game object
        Vector3 gridCenter = new Vector3((gridWidth - 1) * cubeSize / 2f, (gridHeight - 1) * cubeSize / 2f, 0f);
        transform.position = -gridCenter;
    }

    public GameObject playerCubePrefab; // Prefab to use for creating the player cube

    private void SpawnPlayerCube()
    {
        Vector3 playerCubePosition = GetGridPosition(new Vector3(4, 4, 0));
        playerCube = Instantiate(playerCubePrefab, playerCubePosition, Quaternion.identity);
    }
    
    public Vector3 GetGridPosition(Vector3 position)
    {
        int x = Mathf.RoundToInt(position.x / cubeSize);
        int y = Mathf.RoundToInt(position.z / cubeSize);
        return new Vector3(x, y, 0.0f);
    }

    public Vector3 GetWorldPosition(Vector3 gridPosition)
    {
        float x = gridPosition.x * cubeSize + cubeSize / 2.0f;
        float y = 0.0f;
        float z = gridPosition.y * cubeSize + cubeSize / 2.0f;
        return new Vector3(x, y, z);
    }
    
    
}
