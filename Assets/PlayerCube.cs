using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerCube : MonoBehaviour
{
    public Color playerCubeColor; // Color to use for the player cube

    private Color currentColor; // Color of the cubes the player cube is passing through
    private List<GameObject> cubesToChange = new List<GameObject>();

    public Material Material;



    public float playerSpeed = 5f; // Speed of the player cube movement

    public float xMin, xMax, zMin, zMax;

    public Vector2 gridVal;


    public void SetGenerator(GridGenerator generator)
    {
        _generator = generator;
        xMin = generator.NormalCubes[new Vector2(0, 0)].transform.localPosition.x;
        xMax =  generator.NormalCubes[new Vector2(0, _generator.gridSize-1)].transform.localPosition.x;
        zMin = generator.NormalCubes[new Vector2(0, 0)].transform.localPosition.z;
        zMax =  generator.NormalCubes[new Vector2(_generator.gridSize-1,0)].transform.localPosition.z;
    }
    private void Awake()
    {
        // Move the player cube based on input and restrict its position to within the grid area
        //moveDir = Vector3.zero;
        // Calculate the boundaries of the grid area
       
    }

    private Vector3 moveDir;
    public bool left, right, up, down;

    void CheckMov()
    {
        if (moveDir == Vector3.zero && down)
        {
            transform.localPosition = new Vector3(transform.localPosition.x + 0.1f, transform.localPosition.y, transform.localPosition.z);
            down = false;
        }
        else if (moveDir == Vector3.zero && up)
        {
            transform.localPosition = new Vector3(transform.localPosition.x - 0.1f, transform.localPosition.y, transform.localPosition.z);
            up = false;
        }
        else if (moveDir == Vector3.zero && left)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + 0.1f);
            left = false;
        }
        else if (moveDir == Vector3.zero && right )
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z - 0.1f);
            right = false;
        }
        
    }
    private void Update()
    {
        
        /*
        if (Input.GetKey(KeyCode.UpArrow) )
        {
            if(moveDir==Vector3.zero)
                CheckMov();
            moveDir = Vector3.up;
        }
        else if (Input.GetKey(KeyCode.DownArrow) )
        {
           
            if(moveDir==Vector3.zero)
                CheckMov();
            moveDir = Vector3.down;
        }
        else if (Input.GetKey(KeyCode.RightArrow) )
        {
            if(moveDir==Vector3.zero)
                CheckMov();
            moveDir = Vector3.right;
           
        }
        else if (Input.GetKey(KeyCode.LeftArrow) )
        {
            if(moveDir==Vector3.zero)
                CheckMov();
            moveDir = Vector3.left;
        }

        if (transform.localPosition.x > xMax)
        {
            up = true;
        }
        else
        {
          //  up = false;
        }

        if (transform.localPosition.x < xMin)
        {
            down = true;
        }
        else
        {
          //  down = false;
        }

        if (transform.localPosition.z > zMax)
        {
            right = true;
        }
        else
        {
         //   right = false;
        }

        if (transform.localPosition.z < zMin)
        {
            left = true;
        }
        else
        {
        //    left = false;
        }
        
        if (transform.localPosition.x  < xMax && transform.localPosition.x > xMin && transform.localPosition.z  < zMax && transform.localPosition.z  > zMin)
        {
            transform.position += moveDir * playerSpeed * Time.deltaTime;
        }
        else
        {
            
            moveDir = Vector3.zero;
        }*/
       
    }

    private void FixedUpdate()
    {
       
    }

    private bool onTrigger;
    private void OnTriggerEnter(Collider collision)
    {
        
        
        // If the player cube collides with a cube of a different color than the current color,
        // start tracking the cubes to change color
        if (collision.gameObject.CompareTag("GridCube") &&
            collision.gameObject.GetComponent<MeshRenderer>().material != Material)
        {
           
            collision.gameObject.GetComponent<NormalCube>().ChangeMaterial( Material);
           // collision.gameObject.GetComponent<NormalCube>().selected = true;

           // CheckTheSurroundingCubes(collision.gameObject.GetComponent<NormalCube>().gridVal);
            
        }
        
        
         if(collision.gameObject.CompareTag("GridCube") &&
                collision.gameObject.GetComponent<NormalCube>().selected)
        {
            
           // CheckTheSurroundingCubes(collision.gameObject.GetComponent<NormalCube>().gridVal);
            
        }
         
         
        
        

        
       
    }


    public GridGenerator _generator;
    

    void CheckTheSurroundingCubes(Vector2 pos)
    {

        bool found;
       
        if (_generator.NormalCubes[new Vector2(pos.x - 1, pos.y)].selected ||
             _generator.NormalCubes[new Vector2(pos.x + 1, pos.y)].selected)
        {
            if (_generator.NormalCubes[new Vector2(pos.x, pos.y + 1)].selected ||
                _generator.NormalCubes[new Vector2(pos.x, pos.y - 1)].selected)
            {

                found = true;
            }
            else
            {
                found = false;
            }
        }
        else
        {
            found = false;
        }


        if (found)
        {
            for (int i=1;i<pos.x;i++)
            {
                for (int j = 1; i < pos.y; j++)
                {
                    if (_generator.NormalCubes.ContainsKey(new Vector2(i,j)))
                    {
                        var val= _generator.NormalCubes[new Vector2(i, j)];
                        //val.GetComponent<MeshRenderer>().material = Material;
                        val.selected = true;
                    }
                  
                }

            }          
            
        }
        
    }

  

    private GameObject GetCubeAtPosition(Vector3 position)
    {
        Collider[] colliders = Physics.OverlapBox(position, new Vector3(1 / 2f, 1 / 2f, 1 / 2f));
        foreach (Collider collider in colliders)
        {
            if (collider.gameObject.CompareTag("GridCube"))
            {
                return collider.gameObject;
            }
        }
        return null;
    }


    
   
   


    private void OnTriggerExit(Collider collision)
    {
      
        // If the player cube exits the current color area, clear the list of cubes to change color
        if (collision.gameObject.CompareTag("GridCube"))
        {
            onTrigger = false;
          //  cubesToChange.Clear();
        }
    }
}
