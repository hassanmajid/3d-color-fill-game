using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
   [SerializeField] private Transform cameraPos;
   
   public static Action checkifLevelCompleteAction;

   [SerializeField] private Text LevelCompleted;
   

   [SerializeField] private Text levelNumber;
   
   public static GameManager Instance;
   
   public int totalCubesPainted;

   private int levelno = 1;

    public Action playSound;

   
   private void Awake()
   {

      if (Instance != null && Instance != this)
      {
         Destroy(this);
      }
      else
      {
         Instance = this;
      }

   }

   
   private void Start()
   {
        audioSource = this.gameObject.GetComponent<AudioSource>();
      levelNumber.text = "Level " + GetLevel();
      GridGenerator.Instance.GenerateGrid( GetLevel()*5);
      checkifLevelCompleteAction += CheckLevelComplete;
      cameraPos.position = GridGenerator.Instance.playerCube.transform.position;
      cameraPos.position = new Vector3(cameraPos.position.x, cameraPos.position.y, -1*((GetLevel()*5)*2));

        playSound += PlaySoundMethod;

   }

   private void OnDestroy()
   {
      checkifLevelCompleteAction -= CheckLevelComplete;
        playSound -= PlaySoundMethod;
    }

    AudioSource audioSource;


    void PlaySoundMethod()
    {
        audioSource.PlayOneShot(audioSource.clip);
    }

   void CheckLevelComplete()
   {
     
         LevelCompleted.text = "Level " + GetLevel() + " is Completed";
        
         SaveLevel(GetLevel()+1);
        Invoke("LoadNextLevel", 2f);
      
   }


   void LoadNextLevel()
   {
     // LevelCompleted.text = "";
      SceneManager.LoadScene("SampleScene");

   }

   void SaveLevel(int val)
   {
      PlayerPrefs.SetInt("level",val);
   }


   int GetLevel()
   {
      return PlayerPrefs.GetInt("level", 1);
   }
}
